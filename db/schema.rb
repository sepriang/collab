# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150413033914) do

  create_table "account_dashboards", force: :cascade do |t|
    t.integer  "activity_id",   limit: 4
    t.integer  "account_id",    limit: 4
    t.integer  "input_type_id", limit: 4
    t.datetime "deleted_at"
    t.date     "pin_date"
    t.integer  "note_id",       limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "username",               limit: 255
    t.string   "jid_password",           limit: 255
    t.string   "mobile_number",          limit: 255
    t.text     "profile_picture",        limit: 65535
    t.datetime "deleted_at"
  end

  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true, using: :btree
  add_index "accounts", ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true, using: :btree

  create_table "activities", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "deleted_at"
    t.string   "state",       limit: 255
    t.integer  "account_id",  limit: 4
    t.boolean  "is_public",   limit: 1
    t.datetime "approved_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "activity_transactions", force: :cascade do |t|
    t.integer  "activity_id",          limit: 4
    t.integer  "account_id",           limit: 4
    t.integer  "account_dashboard_id", limit: 4
    t.integer  "note_id",              limit: 4
    t.integer  "input_type_id",        limit: 4
    t.decimal  "decimal_score",                    precision: 10
    t.integer  "integer_score",        limit: 4
    t.boolean  "boolean_score",        limit: 1
    t.datetime "deleted_at"
    t.string   "state",                limit: 255
    t.datetime "completed_at"
    t.datetime "archieved_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token", limit: 255
    t.integer  "account_id",   limit: 4
    t.boolean  "active",       limit: 1
    t.datetime "expires_at"
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "images", force: :cascade do |t|
    t.text     "name",        limit: 65535
    t.integer  "activity_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "notes", force: :cascade do |t|
    t.text     "description", limit: 65535
    t.date     "note_for"
    t.datetime "deleted_at"
    t.integer  "account_id",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "description",             limit: 255
    t.integer  "activity_transaction_id", limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

end
