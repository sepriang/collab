class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.text :name
      t.integer :activity_id
      t.timestamps null: false
    end
  end
end
