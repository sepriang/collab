class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.text :token
      t.integer :account_id

      t.timestamps null: false
    end
  end
end
