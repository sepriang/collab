class AddCommentToActivity < ActiveRecord::Migration
  def change
    add_column :activities, :comment, :text
  end
end
