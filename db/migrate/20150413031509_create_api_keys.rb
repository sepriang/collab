class CreateApiKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.string :access_token
      t.integer :account_id
      t.boolean :active
      t.datetime :expires_at
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
