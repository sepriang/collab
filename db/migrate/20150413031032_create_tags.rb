class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :description
      t.integer :activity_transaction_id
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
