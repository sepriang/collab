class AddNewAttrToActivityTransaction < ActiveRecord::Migration
  def change
    add_column :activity_transactions, :last_sync_at, :datetime
    add_column :activity_transactions, :local_id, :integer
  end
end
