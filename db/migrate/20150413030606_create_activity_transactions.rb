class CreateActivityTransactions < ActiveRecord::Migration
  def change
    create_table :activity_transactions do |t|
      t.integer :activity_id
      t.integer :account_id
      t.integer :account_dashboard_id
      t.integer :note_id
      t.integer :input_type_id
      t.decimal :decimal_score
      t.integer :integer_score
      t.boolean :boolean_score
      t.datetime :deleted_at
      t.string :state
      t.datetime :completed_at
      t.datetime :archieved_at

      t.timestamps null: false
    end
  end
end
