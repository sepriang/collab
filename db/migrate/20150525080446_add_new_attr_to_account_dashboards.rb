class AddNewAttrToAccountDashboards < ActiveRecord::Migration
  def change
    add_column :account_dashboards, :local_id, :string
    add_column :account_dashboards, :last_sync_at, :Datetime
  end
end
