class CreateAccountDashboards < ActiveRecord::Migration
  def change
    create_table :account_dashboards do |t|
      t.integer :activity_id
      t.integer :account_id
      t.integer :input_type_id
      t.datetime :deleted_at
      t.date :pin_date
      t.integer :note_id

      t.timestamps null: false
    end
  end
end
