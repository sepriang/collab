class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.text :description
      t.date :note_for
      t.datetime :deleted_at
      t.integer :account_id
      t.timestamps null: false
    end
  end
end
