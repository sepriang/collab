class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.text :description
      t.datetime :deleted_at
      t.string :state
      t.integer :account_id
      t.boolean :is_public
      t.datetime :approved_at
      t.timestamps null: false
    end
  end
end
