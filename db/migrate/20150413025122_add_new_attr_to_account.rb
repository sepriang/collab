class AddNewAttrToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :first_name, :string
    add_column :accounts, :last_name, :string
    add_column :accounts, :username, :string
    add_column :accounts, :jid_password, :string
    add_column :accounts, :mobile_number, :string
    add_column :accounts, :profile_picture, :text
    add_column :accounts, :deleted_at, :datetime
  end
end
