class AddNewAttrToNote < ActiveRecord::Migration
  def change
    add_column :notes, :local_id, :string
    add_column :notes, :last_sync_at, :datetime
  end
end
