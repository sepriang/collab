# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'csv'

ActiveRecord::Base.connection.execute("TRUNCATE TABLE activities")
ActiveRecord::Base.connection.execute("ALTER TABLE activities AUTO_INCREMENT = 1")
CSV.foreach("#{Rails.root}/db/data/activities.csv", :headers => :first_row) do |row|
  row.to_hash.with_indifferent_access
  Activity.create!(row.to_hash.symbolize_keys)
end


