require 'test_helper'

class ActivityTransactionsControllerTest < ActionController::TestCase
  setup do
    @activity_transaction = activity_transactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:activity_transactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create activity_transaction" do
    assert_difference('ActivityTransaction.count') do
      post :create, activity_transaction: { activity_id: @activity_transaction.activity_id, archieved_at: @activity_transaction.archieved_at, boolean_score: @activity_transaction.boolean_score, completed_at: @activity_transaction.completed_at, decimal_score: @activity_transaction.decimal_score, deleted_at: @activity_transaction.deleted_at, input_type_id: @activity_transaction.input_type_id, integer_score: @activity_transaction.integer_score, note_id: @activity_transaction.note_id, state: @activity_transaction.state }
    end

    assert_redirected_to activity_transaction_path(assigns(:activity_transaction))
  end

  test "should show activity_transaction" do
    get :show, id: @activity_transaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @activity_transaction
    assert_response :success
  end

  test "should update activity_transaction" do
    patch :update, id: @activity_transaction, activity_transaction: { activity_id: @activity_transaction.activity_id, archieved_at: @activity_transaction.archieved_at, boolean_score: @activity_transaction.boolean_score, completed_at: @activity_transaction.completed_at, decimal_score: @activity_transaction.decimal_score, deleted_at: @activity_transaction.deleted_at, input_type_id: @activity_transaction.input_type_id, integer_score: @activity_transaction.integer_score, note_id: @activity_transaction.note_id, state: @activity_transaction.state }
    assert_redirected_to activity_transaction_path(assigns(:activity_transaction))
  end

  test "should destroy activity_transaction" do
    assert_difference('ActivityTransaction.count', -1) do
      delete :destroy, id: @activity_transaction
    end

    assert_redirected_to activity_transactions_path
  end
end
