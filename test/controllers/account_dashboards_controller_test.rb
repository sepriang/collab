require 'test_helper'

class AccountDashboardsControllerTest < ActionController::TestCase
  setup do
    @account_dashboard = account_dashboards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:account_dashboards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create account_dashboard" do
    assert_difference('AccountDashboard.count') do
      post :create, account_dashboard: { account_id: @account_dashboard.account_id, activity_id: @account_dashboard.activity_id, deleted_at: @account_dashboard.deleted_at, input_type_id: @account_dashboard.input_type_id, note_id: @account_dashboard.note_id }
    end

    assert_redirected_to account_dashboard_path(assigns(:account_dashboard))
  end

  test "should show account_dashboard" do
    get :show, id: @account_dashboard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @account_dashboard
    assert_response :success
  end

  test "should update account_dashboard" do
    patch :update, id: @account_dashboard, account_dashboard: { account_id: @account_dashboard.account_id, activity_id: @account_dashboard.activity_id, deleted_at: @account_dashboard.deleted_at, input_type_id: @account_dashboard.input_type_id, note_id: @account_dashboard.note_id }
    assert_redirected_to account_dashboard_path(assigns(:account_dashboard))
  end

  test "should destroy account_dashboard" do
    assert_difference('AccountDashboard.count', -1) do
      delete :destroy, id: @account_dashboard
    end

    assert_redirected_to account_dashboards_path
  end
end
