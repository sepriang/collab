class AccountDashboardsController < ApplicationController
  before_action :set_account_dashboard, only: [:show, :edit, :update, :destroy]

  # GET /account_dashboards
  # GET /account_dashboards.json
  def index
    @account_dashboards = AccountDashboard.all
  end

  # GET /account_dashboards/1
  # GET /account_dashboards/1.json
  def show
  end

  # GET /account_dashboards/new
  def new
    @account_dashboard = AccountDashboard.new
  end

  # GET /account_dashboards/1/edit
  def edit
  end

  # POST /account_dashboards
  # POST /account_dashboards.json
  def create
    @account_dashboard = AccountDashboard.new(account_dashboard_params)

    respond_to do |format|
      if @account_dashboard.save
        format.html { redirect_to @account_dashboard, notice: 'Account dashboard was successfully created.' }
        format.json { render :show, status: :created, location: @account_dashboard }
      else
        format.html { render :new }
        format.json { render json: @account_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /account_dashboards/1
  # PATCH/PUT /account_dashboards/1.json
  def update
    respond_to do |format|
      if @account_dashboard.update(account_dashboard_params)
        format.html { redirect_to @account_dashboard, notice: 'Account dashboard was successfully updated.' }
        format.json { render :show, status: :ok, location: @account_dashboard }
      else
        format.html { render :edit }
        format.json { render json: @account_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /account_dashboards/1
  # DELETE /account_dashboards/1.json
  def destroy
    @account_dashboard.destroy
    respond_to do |format|
      format.html { redirect_to account_dashboards_url, notice: 'Account dashboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account_dashboard
      @account_dashboard = AccountDashboard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_dashboard_params
      params.require(:account_dashboard).permit(:activity_id, :account_id, :input_type_id, :deleted_at, :note_id)
    end
end
