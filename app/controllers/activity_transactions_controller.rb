class ActivityTransactionsController < ApplicationController
  before_action :set_activity_transaction, only: [:show, :edit, :update, :destroy]

  # GET /activity_transactions
  # GET /activity_transactions.json
  def index
    @activity_transactions = ActivityTransaction.all
  end

  # GET /activity_transactions/1
  # GET /activity_transactions/1.json
  def show
  end

  # GET /activity_transactions/new
  def new
    @activity_transaction = ActivityTransaction.new
  end

  # GET /activity_transactions/1/edit
  def edit
  end

  # POST /activity_transactions
  # POST /activity_transactions.json
  def create
    @activity_transaction = ActivityTransaction.new(activity_transaction_params)

    respond_to do |format|
      if @activity_transaction.save
        format.html { redirect_to @activity_transaction, notice: 'Activity transaction was successfully created.' }
        format.json { render :show, status: :created, location: @activity_transaction }
      else
        format.html { render :new }
        format.json { render json: @activity_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activity_transactions/1
  # PATCH/PUT /activity_transactions/1.json
  def update
    respond_to do |format|
      if @activity_transaction.update(activity_transaction_params)
        format.html { redirect_to @activity_transaction, notice: 'Activity transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @activity_transaction }
      else
        format.html { render :edit }
        format.json { render json: @activity_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activity_transactions/1
  # DELETE /activity_transactions/1.json
  def destroy
    @activity_transaction.destroy
    respond_to do |format|
      format.html { redirect_to activity_transactions_url, notice: 'Activity transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activity_transaction
      @activity_transaction = ActivityTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activity_transaction_params
      params.require(:activity_transaction).permit(:activity_id, :note_id, :input_type_id, :decimal_score, :integer_score, :boolean_score, :deleted_at, :state, :completed_at, :archieved_at)
    end
end
