json.array!(@api_keys) do |api_key|
  json.extract! api_key, :id, :access_token, :account_id, :state, :expires_at, :deleted_at
  json.url api_key_url(api_key, format: :json)
end
