json.array!(@account_dashboards) do |account_dashboard|
  json.extract! account_dashboard, :id, :activity_id, :account_id, :input_type_id, :deleted_at, :note_id
  json.url account_dashboard_url(account_dashboard, format: :json)
end
