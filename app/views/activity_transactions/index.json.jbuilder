json.array!(@activity_transactions) do |activity_transaction|
  json.extract! activity_transaction, :id, :activity_id, :note_id, :input_type_id, :decimal_score, :integer_score, :boolean_score, :deleted_at, :state, :completed_at, :archieved_at
  json.url activity_transaction_url(activity_transaction, format: :json)
end
