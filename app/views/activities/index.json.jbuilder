json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :description, :deleted_at, :state
  json.url activity_url(activity, format: :json)
end
