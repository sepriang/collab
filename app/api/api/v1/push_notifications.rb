module API
  module V1
    class PushNotifications < Grape::API
      version 'v1'
      format :json
      
      	resource :notifications do
        	desc "Send push notifications"
        	
        	before  do
          		authenticated!
        	end

        	params do
	        	requires :username, type: String, desc: "username"
	        	requires :message, type: String, desc: "Message"
			end

        	post :send do        	
          		friend = Account.find_by_username(params[:username])
          		if friend
	  	      		pusher = Grocer.pusher(certificate: "#{Rails.root}/lib/ck.pem", passphrase:  "C011@b",gateway: 'gateway.sandbox.push.apple.com')
			  		message = params[:message]	
			  		alert = ''
			  		if current_account.first_name
			  			alert = "#{current_account.first_name}: #{message}"
			  		else
			  			alert = "#{current_account.username}: #{message}"
			  		end      
  	      	  		notification = Grocer::Notification.new(
					  	device_token:      friend.device.token.to_s,
					  	alert:             alert,
					  	badge:             1,
					  	sound:             "siren.aiff",
					  	custom: {
						    buddy: "#{friend.username}"
						}
		  	  		)
			  		pusher.push(notification)	
			  		status 201
	          		{ :success => true, :info => "Push message to user",:data=> nil}
          		else
          	 		status 201
	          		{ :success => true, :info => "Invalid account",:data=> nil}
          		end
        	end
        end

        helpers do
        	def authenticated!
          		error!('Unauthorized. Invalid or expired token.', 401) unless current_account
        	end

        	def current_account
	          # find token. Check if valid.
	          token = ApiKey.where(access_token: headers["Access-Token"]).first
	          if token && !token.expired?
	            @current_account = Account.find(token.account_id)
	          end
	        end
      	end

    end
  end
end
