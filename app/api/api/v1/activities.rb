module API
  module V1
    class Activities < Grape::API
      version 'v1'
      format :json


      #Preferred json
        # { login : 'test@gmail.com or username', password: 'password'}
      resource :activities do
        desc "Activity List"
        before  do
          authenticated!
        end
        get :all do        	
          activities = Activity.where("(activities.is_public = true AND activities.state = 'approved') OR activities.account_id = ?",@current_account.id)
          activity_list = []
          activities.each do |activity|
            obj = JSON.parse(activity.to_json)
            obj.delete("description")
            images = []
            activity.images.each do |image|
              images << image.name_url(:detail)
            end
            obj["images"] = images
            obj["preview_image"] = activity.images.first.name_url(:thumb) unless activity.images.first.nil?
            obj['activity_description'] = activity.description
            activity_list << obj
          end
  		    status 201
          { :success => true, :info => "List of activities",:data=> activity_list}
        end
      end

      resource :input_types do
        desc "Input Type List"
            before  do
              authenticated!
            end
        get :all do        
          obj = JSON.parse(Activity.input_types.to_json)
          input_types = []
          obj.each do |json_input|
            input_type = Hash.new
            input_type["id"] = json_input[0]
            input_type["name"] = json_input[1]["name"]
            input_type["data_type"] = json_input[1]["data_type"]
            input_types << input_type
          end
          
          status 201
          { :success => true, :info => "List of input types",:data=> input_types}
        end
      end

      resource :activities do
        desc "Activity Detail"
        before  do
          authenticated!
        end
        params do
          requires :id, type: Integer, desc: "activity id"
        end
        get :detail do         
          activity = Activity.find_by_id(params[:id].to_i)
          obj = JSON.parse(activity.to_json)
            obj.delete("description")
            obj["images"] = activity.images
            obj['activity_description'] = activity.description
            status 201
          status 201
          { :success => true, :info => "activity detail",:data=> obj}
        end
      end

      resource :activities do
        desc "Create new Activity"
        before  do
          authenticated!
        end
        params do
          requires :activity , type: Hash, desc: "Activity" do
            requires :name, type: String, desc: "name of activity", allow_blank: false
            requires :activity_description, type: String, desc: "Description of activity", allow_blank: false
          end
        end
        post :create do 
          activity = Activity.new(name: params[:activity][:name],description: params[:activity][:activity_description],is_public: params[:activity][:is_public],account_id: @current_account.id,state: "pending")
          if activity.save!
            (1..4).each do |i|
              unless params["image#{i}".to_sym].nil?
                img =Image.create(name: params["image#{i}".to_sym],activity_id: activity.id);
              end
            end
            obj = JSON.parse(activity.to_json)
            obj.delete("description")
            obj['activity_description'] = activity.description
            status 201
            { :success => true, :info => "activity is created",:data=> obj}
          else
            status 403
            { :success => false, :info => "fail to create activity",:data=> nil}
          end
        end
      end

      resource :activities do
        desc "Update Activity"
        before  do
          authenticated!
        end
        params do
          requires :activity , type: Hash, desc: "Activity" do
            requires :name, type: String, desc: "Name", allow_blank: false
            requires :activity_description, type: String, desc: "Description", allow_blank: false
          end
        end
        post :update do   
             
          activity = Activity.find_by_id(params[:activity][:id])
          activity.name = params[:activity][:name]
          activity.description = params[:activity][:activity_description]
          activity.is_public = params[:activity][:is_public].to_i
          if activity.save
            if params["image1".to_sym]
              activity.images.delete_all
            end
            puts "X"*200
            puts params[:activity] 
            puts activity.is_public
            images = []
            (1..4).each do |i|
              unless params["image#{i}".to_sym].nil?
                img =Image.create(name: params["image#{i}".to_sym],activity_id: activity.id);
                images << img
                 puts "h"*200
                 puts img.activity.id
              end
            end


         

            obj = JSON.parse(activity.to_json)
            obj.delete("description")
            obj['activity_description'] = activity.description
            status 201
            { :success => true, :info => "activity is updated",:data=> obj}
          else
            status 403
            { :success => false, :info => "fail to update activity",:data=> nil}
          end
        end
      end

      resource :activities do
        desc "Add Activity To dashboard"
        before  do
          authenticated!
        end
        params do
          requires :activity_dashboard , type: Hash, desc: "ActivityDashboard" do
            requires :activity_id, type: Integer, desc: "Activity ID", allow_blank: false
            requires :note, type: String, desc: "Note", allow_blank: true
            requires :input_type_id, type: Integer, desc: "InputType Id", allow_blank: false
          end
        end
        get :add_to_dashboard do         
          activity = Activity.find_by_id(params[:activity_dashboard][:activity_id].to_i)

          activity_dashboard = ActivityDashboard.new(activity_id: activity.id,account_id: @current_account.id,input_type_id: params[:activity_dashboard][:input_type_id])
          activity_dashboard.note.build(description: params[:activity_dashboard][:note]);
          if activity_dashboard.save
            status 201
            { :success => true, :info => "successfully added to dashboard",:data=> activity}
          else
            status 403
            { :success => false, :info => "fail to add to dashboard",:data=> nil}
          end
        end
      end

      resource :activities do
        desc "Activity List not on dashboard"
        before  do
          authenticated!
        end
        get :not_on_dashboard do         
          activities = Activity.where("activities.id not in (?)",@current_account.activities.map(&:id))
          status 201
          { :success => true, :info => "List of activities not on dashboard",:data=> activities}
        end
      end

      resource :activities do
        desc "Activity on dashboard"
        before  do
          authenticated!
        end
        get :on_dashboard do         
          activities = @current_account.activities
          status 201
          { :success => true, :info => "List of activities not on dashboard",:data=> activities}
        end
      end

      resource :activities do
        desc "Activity on dashboard by date"
        before  do
          authenticated!
        end
        params do
          requires :date, type: Date, desc: "Date", allow_blank: false
        end
        get :on_dashboard_by_date do         
          activities = @current_account.activities.where(params[:date])
          status 201
          { :success => true, :info => "List of activities not on dashboard",:data=> activities}
        end
      end



      resource :activities do
        desc "remove on dashboard"
        before  do
          authenticated!
        end
        params do
          requires :activity_id, type: Integer, desc: "Activity ID", allow_blank: false
        end
        post :remove_on_dashboard do         
          activity = ActivityDashboard.find_by_activity_id(params[:activity_id])
          if activity.delete
            status 201
            { :success => true, :info => "Remove from dashboard",data: nil}
          else
            status 405
            { :success => false, :info => "Fails to remove",:data=> nil}
          end
        end
      end

      resource :activities do
        desc "Activity on dashboard"
        before  do
          authenticated!
        end
        get :not_on_dashboard do         
          activities = current_account.activities
          status 201
          { :success => true, :info => "List of activities not on dashboard",:data=> activities}
        end
      end


      helpers do
        def authenticated!
          error!('Unauthorized. Invalid or expired token.', 401) unless current_account
        end

        def current_account
          # find token. Check if valid.
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
            if @current_account
              true
            else
              false
            end
          else
            false
          end
        end
      end


    end
  end
end
