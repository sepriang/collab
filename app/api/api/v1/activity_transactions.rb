require 'csv'
module API
  module V1
    class ActivityTransactions < Grape::API
      version 'v1'
      format :json


      #Preferred json
        # { login : 'test@gmail.com or username', password: 'password'}
      resource :activity_transactions do
        before  do
          authenticated!
        end
        params do
          requires :activity_transaction , type: Hash, desc: "ActivityTransaction" do
            requires :note, type: String, desc: "Note", allow_blank: true
            requires :account_dashboard_id, type: Integer, desc: "Dashboard id", allow_blank: true
            requires :decimal_score, type: Float, desc: "Score", allow_blank: true
            requires :boolean_score, type: Integer, desc: "Score", allow_blank: true
            requires :integer_score, type: Integer, desc: "Score", allow_blank: true
          end
        end
        post :done do
        	dashboard = @current_account.account_dashboards.where('account_dashboards.id = ? ',params[:activity_transaction][:account_dashboard_id].to_i).first
        	if dashboard
            activity_transaction = dashboard.activity_transaction
            if activity_transaction
              activity_transaction.integer_score =  params[:activity_transaction][:integer_score]
              activity_transaction.decimal_score =  params[:activity_transaction][:decimal_score]
              activity_transaction.boolean_score = params[:activity_transaction][:boolean_score]
              activity_transaction.completed_at = Date.today
              if activity_transaction.save
                 status 201
                    { :success => true, :info => "Transaction updated",:data=> activity_transaction}
              else
                 status 405
                  { :success => false, :info => "Unable to submit transaction"}
              end
            else
        		    activity_transaction = ActivityTransaction.new(activity_id: dashboard.activity_id,input_type_id: dashboard.input_type_id,
        													 account_id: @current_account.id,account_dashboard_id: dashboard.id,integer_score: params[:activity_transaction][:integer_score],
                                   boolean_score: params[:activity_transaction][:boolean_score],decimal_score: params[:activity_transaction][:decimal_score],completed_at: Date.today)
        	      note = Note.new(description:  params[:activity_transaction][:note])
                if activity_transaction && note
                  if activity_transaction.save && note.save
                    activity_transaction.note_id = note.id
                    status 201
                    { :success => true, :info => "Transaction submitted",:data=> activity_transaction}
                  else
                      status 405
                      { :success => false, :info => "Unable to submit transaction"}
                  end
                else
                  status 405
                  { :success => false, :info => "Unable to submit transaction"}
                end
            end
          else
            puts "null-d"*200
        	  status 405
            { :success => false, :info => "Unable to submit transaction"}
        	end

          
        end
      end

      resource :activity_transactions do
        before  do
          authenticated!
        end
        params do
            requires :account_dashboard_id, type: String, desc: "Dashboard id", allow_blank: true
        end
        get :show do
          account_dashboard =  @current_account.account_dashboards.find_by_id(params[:account_dashboard_id]) if @current_account
          if account_dashboard
              obj = JSON.parse(account_dashboard.to_json)
                activity =account_dashboard.activity
                obj_activity = JSON.parse(activity.to_json)
                obj_activity.delete("description")
                obj_activity["images"] = activity.images
                obj_activity['activity_description'] = activity.description
              obj["activity"] = obj_activity  
              obj["note"] = account_dashboard.note.description if account_dashboard.note
              obj["input_type"]= Activity.input_types[account_dashboard.input_type_id]
              obj["activity_transaction"]= account_dashboard.activity_transaction
              if account_dashboard.activity_transaction
                 obj_note = JSON.parse( account_dashboard.activity_transaction.note.to_json)
                 obj_note.delete('description')
                 obj_note["note_description"] = note.description
                 obj["activity_transaction"].delete('note')
                 obj["activity_transaction"]["note"] = obj_note 
              end
             

            status 201
                { :success => true, :info => "Transaction",:data=> obj}
          else
            status 405
                { :success => true, :info => "No transaction",:data=> nil}
          end
        end
      end

      resource :activity_transactions do
          before  do
            authenticated!
          end
        params do
            requires :pin_date, type: Date, desc: "Date", allow_blank: true
        end
        get :by_date do
          account_dashboards =  @current_account.account_dashboards.where(pin_date: params[:pin_date]) if @current_account
          if account_dashboards
            account_dashboards_list = []
            account_dashboards.each do |account_dashboard|
              obj = JSON.parse(account_dashboard.to_json)
                activity =account_dashboard.activity
                obj_activity = JSON.parse(activity.to_json)
                obj_activity.delete("description")
                obj_activity["images"] = activity.images
                obj_activity['activity_description'] = activity.description
              obj["activity"] = obj_activity  
              obj["input_type"]= Activity.input_types[account_dashboard.input_type_id]
              obj["activity_transaction"]= account_dashboard.activity_transaction
              account_dashboards_list << obj
            end
            status 201
                { :success => true, :info => "Activity List",:data=> account_dashboards_list}
          else
            status 201
                { :success => false, :info => "No Activites",:data=> []}
          end
        end
      end

      #Pin sync acitivity
      resource :activites do
        before  do
          authenticated!
        end
        post :sync_pin_activities do
            puts "X"*200
            puts params["account_dashboards"]
            puts @current_account.username
            account_dashboards = params["account_dashboards"];
            existing_array = AccountDashboard.all.map(&:id);
            
            new_account_dashboards = account_dashboards.select { |ac| !existing_array.include? ac['id'] } if account_dashboards
            new_account_dashboards.each do |nad|
              nad["last_sync_at"] = Date.today
              nad["account_id"] = @current_account.id
              nad.delete('is_deleted');
              nad.delete('id');
            end
            
            if existing_array 
              md_account_dashboards = account_dashboards.select {|ad| existing_array.include? ad["id"] }
            end
            
            ActiveRecord::Base.transaction do
                AccountDashboard.import!(new_account_dashboards)
            end if new_account_dashboards

            md_account_dashboards.each do |mad|
              dad = AccountDashboard.find_by_id(mad["id"].to_s)
              if mad["is_deleted"] == '1'
                dad.delete
              else
                dad.last_sync_at = Date.today
                dad.save
              end
            end

            account_dashboard_list = @current_account.account_dashboards.where("local_id is NOT NULL");
            status 201
            { :success => true, :info => "Transaction pinned",:data=>account_dashboard_list}
          
        end
      end

      #Pin sync acitivity
      resource :activites do
        before  do
          authenticated!
        end
        post :sync_notes do
            puts "X"*200
            puts params["notes"]
            notes = params["notes"];
            existing_array = Note.all.map(&:id);
            
            new_notes = notes.select { |ac| !existing_array.include? ac['id'] } if notes
            new_notes.each do |note|
              note["last_sync_at"] = Date.today
              note["account_id"] = @current_account.id
              note["description"] = note["note_description"];
              note.delete('note_description');
              note.delete('is_deleted');
              note.delete('id');
            end
            
            if existing_array 
              md_notes = notes.select {|ad| existing_array.include? ad["id"] }
            end
            
            ActiveRecord::Base.transaction do
                Note.import!(new_notes)
            end if new_notes

            md_notes.each do |note|
              m_note = Note.find_by_id(note["id"].to_s)
              if m_note["is_deleted"] == '1'
                m_note.delete
              else
                m_note.last_sync_at = Date.today
                m_note.save
              end
            end

            note_list = @current_account.notes.where("local_id is NOT NULL");
            status 201
            { :success => true, :info => "Sync notes",:data=>note_list}
          
        end
      end

      #Sync acitivity transactions
      resource :activites do
        before  do
          authenticated!
        end
        post :sync_activity_transactions do
            activity_transactions = params["activity_transactions"];
            existing_array = ActivityTransaction.all.map(&:id)
            new_activity_transactions = []
            activity_transactions.each do |ac|
              if !existing_array.include? ac['id'].to_i
                puts ac
                new_activity_transactions << ac
              end
            end
            new_activity_transactions.each do |nad|
              nad["last_sync_at"] = Date.today
              nad["account_id"] = @current_account.id
              nad.delete('is_deleted');
              nad.delete('id');
            end


            if existing_array 
              md_activity_transactions = activity_transactions.select {|ad| existing_array.include? ad["id"].to_i }
            end

            ActiveRecord::Base.transaction do
                ActivityTransaction.import!(new_activity_transactions)
            end if new_activity_transactions

            md_activity_transactions.each do |mad|
              dad = ActivityTransaction.find_by_id(mad["id"].to_s)
              if mad["is_deleted "] == '1'
                dad.delete
              else
                dad.last_sync_at = Date.today
                dad.integer_score = mad["integer_score"]
                dad.decimal_score = mad["decimal_score"]
                dad.boolean_score = mad["boolean_score"]
                dad.save
              end
            end

            activity_transaction_list = @current_account.activity_transactions.where("local_id is NOT NULL");
            status 201
            { :success => true, :info => "Transaction pinned",:data=>activity_transaction_list}
          
        end
      end


      #Pin new acitivity
      resource :activites do
        before  do
          authenticated!
        end
        params do
          requires :account_dashboard , type: Hash, desc: "Account Dashboard" do
            requires :activity_id, type: Integer, desc: "Activity ID", allow_blank: false
            requires :note, type: String, desc: "Note", allow_blank: true
            requires :pin_date, type: Date, desc: "Date ", allow_blank: true
            requires :input_type_id, type: Integer, desc: "Input Type", allow_blank: false
          end
        end
        post :pin do
            account_dashboard= AccountDashboard.new(activity_id: params[:account_dashboard][:activity_id],pin_date: params[:account_dashboard][:pin_date],input_type_id: params[:account_dashboard][:input_type_id])
            account_dashboard.account_id = @current_account.id
            note = Note.new(description: params[:account_dashboard][:note])
            if account_dashboard.save && note.save
               account_dashboard.note_id = note.id
               account_dashboard.save
              status 201
              { :success => true, :info => "Transaction pinned",:data=> account_dashboard}
            else
                  status 405
                  { :success => false, :info => "Unable to pin transaction"}
            end
        end
      end

      resource :activites do
        before  do
          authenticated!
        end
        params do
          requires :account_dashboard_id, type: Integer, desc: "account_dashboard ID", allow_blank: false
        end
        post :unpin do
            account_dashboard= AccountDashboard.find(params[:account_dashboard_id])
            if account_dashboard.delete
                status 201
                  { :success => true, :info => "Transaction unpinned",:data=> account_dashboard}
            else
                  status 405
                  { :success => false, :info => "Unable to unpin transaction"}
            end
        end
      end

      resource :activites do
        before  do
          authenticated!
        end
        params do
          requires :account_dashboard , type: Hash, desc: "Account Dashboard" do
            requires :account_dashboard_id, type: Integer, desc: "account_dashboard ID", allow_blank: false
            requires :activity_id, type: Integer, desc: "Activity ID", allow_blank: false
            requires :note, type: String, desc: "Note", allow_blank: true
            requires :pin_date, type: Date, desc: "Date ", allow_blank: true
            requires :input_type_id, type: Integer, desc: "Input Type", allow_blank: false
          end
        end
        post :update_pin do
            account_dashboard= AccountDashboard.find(params[:account_dashboard][:account_dashboard_id])
            if account_dashboard
               account_dashboard.note.delete

               if account_dashboard.update_attributes(activity_id: params[:account_dashboard][:activity_id],pin_date: params[:account_dashboard][:pin_date],input_type_id: params[:account_dashboard][:input_type_id])
                  account_dashboard.note.build(description: params[:account_dashboard][:note])
                  status 201
                  { :success => true, :info => "Transaction pinned",:data=> account_dashboard}
              else
                  status 405
                  { :success => false, :info => "Unable to pin transaction"}
              end
            else
              status 405
              { :success => false, :info => "Unable to pin transaction"}
            end
        end
      end

      resource :note do
        before  do
          authenticated!
        end
        params do
          requires :note , type: Hash, desc: "Note" do
            requires :note_description, type: String, desc: "about the note", allow_blank: false
            # requires :note_for, type: String, desc: "date for note", allow_blank: true
          end
        end
        post :add do
            note= Note.new(description: params[:note][:note_description],note_for: params[:note][:note_for])
            puts note
            note.account_id = @current_account.id if @current_account
            if note.save
               obj = JSON.parse(note.to_json)
                 obj.delete('description')
                obj['note_description'] = note.description
                status 201
                  { :success => true, :info => "Create note",:data=> obj}

            else
                  status 405
                  { :success => false, :info => "Fail to create note"}
            end
        end
      end

       resource :note do
        before  do
          authenticated!
        end
        params do
          requires :note , type: Hash, desc: "Note" do
            requires :id, type: String, desc: "id the note", allow_blank: false
            requires :note_description, type: String, desc: "about the note", allow_blank: false
          end
        end
        post :update do
            note= Note.find(params[:note][:id])
            note.description = params[:note][:note_description]
            note.note_for = Date.today
            if note.save
                obj = JSON.parse(note.to_json)
                obj.delete('description')
                obj['note_description'] = note.description
                status 201
                  { :success => true, :info => "note Updated",:data=> obj}
            else
                  status 405
                  { :success => false, :info => "Fail to create note"}
            end
        end
      end


      resource :account_dashboards do
        before  do
          authenticated!
        end
        params do
          requires :note, type: String, desc: "about the note", allow_blank: false
          requires :account_dashboard_id, type: String, desc: "account_dashboard_id", allow_blank: false
        end
        post :note do
              account_dashboard = AccountDashboard.find(params[:account_dashboard_id])
             if account_dashboard
               if account_dashboard.note
                  note = account_dashboard.note
                  note.update_attributes(description: params[:note])
                  if note.save
                    status 201
                    { :success => true, :info => "Note Updated",:data=> note}
                  else
                    status 405
                     { :success => false, :info => "Fail to update note"}
                  end
               else
                  note= Note.new(description: params[:note],note_for: Date.today)
                  note.account_id = @current_account.id if @current_account
                  if note.save
                    account_dashboard.note_id = note.id
                    if account_dashboard.save
                      status 201
                      { :success => true, :info => "Create note",:data=> note}
                    else
                      status 405
                      { :success => false, :info => "Fail to create note"}
                    end
                  else
                    status 405
                    { :success => false, :info => "Fail to create note"}
                  end
               end
             end  
        end
      end

      resource :note do
        before  do
          authenticated!
        end
        params do
          requires :id, type: String, desc: "about the node", allow_blank: false
        end
        get :show do
            note= Note.find(params[:id])
            if note
                status 201
                  { :success => true, :info => "Create note",:data=> note}
            else
                  status 405
                  { :success => false, :info => "Fail to create note"}
            end
        end
      end

      resource :notes do
        before  do
          authenticated!
        end
        params do
          requires :note_for, type: Date, desc: "note date", allow_blank: false
        end
        get :by_date do
            note_for = params[:note_for]

            notes= @current_account.notes.where(:note_for => note_for.beginning_of_day..note_for.end_of_day) if @current_account
            note_list = []
            notes.each do |note|
              obj = JSON.parse(note.to_json)
              obj.delete('description');
              obj['note_description'] = note.description
              note_list << obj
            end
            status 201
            { :success => true, :info => "Create note",:data=> note_list}
        end
      end


      helpers do
        def authenticated!
          error!('Unauthorized. Invalid or expired token.', 401) unless current_account
        end

        def current_account
          # find token. Check if valid.
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
          end
        end
      end

    end
  end
end
