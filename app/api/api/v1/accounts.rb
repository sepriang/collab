module API
  module V1
    class Accounts < Grape::API
      version 'v1'
      format :json


      #Preferred json
        # { login : 'test@gmail.com or username', password: 'password'}
      resource :account do
        desc "Account Profile"
        params do
          requires :account , type: Hash, desc: "Acount" do
            requires :username, type: String, desc: "emails"
          end
        end
        before  do
          authenticated!
        end
        get :profile do        	
          account = Account.find_by_username(params[:account][:username])
          if account
             obj = JSON.parse(account.to_json)
             obj.delete('profile_picture')
             obj['profile_picture'] = account.profile_picture_url(:chat);
             obj["api_key"] = account.api_keys.first
             obj["jidStr"] = "#{account.username}@localhost"
             status 201
             { :success => true, :info => "Account profile",:data=> obj}
          else
            status 403
             { :success => true, :info => "Invalid profile",:data=> nil}
          end
        end
      end

      resource :account do
        desc "Account Profile"
        params do
          requires :username, type: String, desc: "username"
        end
        before  do
          authenticated!
        end
        get :get_profile_picture do         
          account = Account.find_by_username(params[:username])
          if account
             obj = JSON.parse(account.to_json)
             obj.delete('profile_picture')
             obj['profile_picture'] = account.profile_picture_url(:chat);
             obj["api_key"] = account.api_keys.first
             obj["jidStr"] = "#{account.username}@localhost"
             status 201
             { :success => true, :info => "Account profile",:data=> obj}
          else
            status 403
             { :success => true, :info => "Invalid profile",:data=> nil}
          end
        end
      end

      resource :account do
        desc "Account Profile"
        before  do
          authenticated!
        end
        post :buddy_profile do     
          buddies = Account.where(username: params[:usernames])
          if buddies
              buddy_list = []
              buddies.each do |buddy|
                obj = JSON.parse(buddy.to_json)
                obj.delete('profile_picture')
                obj['profile_picture'] = buddy.profile_picture_url(:chat);
                obj["api_key"] = buddy.api_keys.first
                obj["jidStr"] = "#{buddy.username}@localhost"
                buddy_list << obj
             end
              status 201
              { :success => true, :info => "Buddies",:data=> buddy_list}
          else
            status 403
            { :success => true, :info => "Buddies",:data=> nil}
          end
        end
      end


       resource :profile do
        desc "Edit Profile"
        before  do
          authenticated!
        end
        
        params do
          requires :account , type: Hash, desc: "Acount" do
            requires :first_name, type: String, desc: "First Name "
            requires :last_name, type: String, desc: "Last Name "
            requires :mobile_number, type: String, desc: "Mobile Number "
          end
        end  
        post :edit do     
          account = @current_account   
          if account.update_attributes(first_name: params[:account][:first_name],last_name: params[:account][:last_name],mobile_number: params[:account][:mobile_number],profile_picture: params[:profile_picture])
             obj = JSON.parse(account.to_json)
             obj.delete('profile_picture')
             obj['profile_picture'] = account.profile_picture_url(:chat);
             obj["api_key"] = account.api_keys.first
             obj["jidStr"] = "#{account.username}@localhost"
             status 201
            { :success => true, :info => "Account profile",:data=> obj}
          else
              status 405
            { :success => false, :info => "Account profile",:data=> nil}
          end
        end
      end

       resource :device do
        desc "Set Device token"
        before  do
          authenticated!
        end
        
        params do
          requires :device , type: Hash, desc: "device" do
            requires :token, type: String, desc: "Device token "
          end
        end  
        post :set do     
          account = @current_account   
          if account
             account.device.delete if account.device
             device = Device.create!(token: params[:device][:token],account_id: account.id) 
             if device
                status 201
                { :success => true, :info => "Fails to set device",:data=> device}
             else
                status 405
                { :success => false, :info => "Fail to set device",:data=> device}
             end
          else
            status 405
            { :success => false, :info => "Fail to set device",:data=> nil}
          end
        end
      end

      resource :account do
        desc "remove contact"
        before  do
          authenticated!
        end
        params do 
          requires :c, type: String, desc: "username of the buddy"
        end  
        post :remove_contact do
          account = Account.find_by_username(params[:username])
          if account
           @current_account.remove_ejabberd_buddy(account)
            status 201
            { :success => true, :info => "Ready to chat"}
          else
            status 403
            { :success => true, :info => "Valid account"}
          end
        end
      end

      resource :account do
        desc "Chat with admin"
        before  do
          authenticated!
        end
        post :chat_with_admin do
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
          end
          @current_account.connect_to_admin
          status 201
          { :success => true, :info => "Ready to chat",:data=> @current_account}
        end
      end

      resource :account do
        desc "Change password"
        before  do
          authenticated!
        end
        params do 
          requires :current_password, type: String, desc: "Current Password"
          requires :password, type: String, desc: "New Password"
          requires :password_confirmation, type: String, desc: "New Password Confirmation"
        end
        post :change_password do         
          account = @current_account
          if account.valid_password?(params[:current_password])
            account.password = params[:password]
            account.password_confirmation = params[:password_confirmation]
            if account.save
              obj = JSON.parse(account.to_json);
              obj.delete('profile_picture') 
              obj['profile_picture'] = account.profile_picture_url(:chat) 
              obj["api_key"] = account.api_keys.first
              obj["jidStr"] = "#{account.username}@localhost"
              status 201
              { :success => true, :info => "Successfully changed password",:data=> account}
            else
              status 403
              { :success => false, :info => "Fails to change password",:data=> nil}
            end
          else
            status 403
            { :success => false, :info => "Invalid password",data: nil}
          end
        end
      end

      resource :account do
        desc "Forgot password"
        params do 
          requires :email, type: String, desc: "Current Password"
        end
        post :forgot_password do         
          account = Account.find_by_email(params[:email])
          if account
            status 201
            { :success => true, :info => "You will receive email",:data=> account}
          else
            status 403
            { :success => false, :info => "Fails",data: nil}
          end
        end
      end

      resource :account do
        desc "Add new contact"
        before do
          authenticated!
        end
        params do 
          requires :account , type: Hash, desc: "Acount" do
            requires :email, type: String, desc: "Email of new buddy"
          end
        end    
        post :add_new_contact do
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
          end
          account = Account.find_by_email(params[:account][:email])
          if @current_account
            if account
              obj = JSON.parse(account.to_json)
              obj.delete('profile_picture')
              obj['profile_picture'] = account.profile_picture_url(:chat);
              obj["api_key"] = account.api_keys.first
              obj["jidStr"] = "#{account.username}@localhost"
              status 201
              status 201
              { :success => true, :info => "Request new buddy",data: obj}
            else
              status 403
              { :success => false, :info => "This user is not in our system. We invited by email",data: nil}
            end
          else
            status 401
            { :success => false, :info => "Unauthorized. Invalid or expired token",data: nil}
          end
        end
      end


     
      helpers do
        def authenticated!
          error!('Unauthorized. Invalid or expired token.', 401) unless current_account
        end

        def current_account
          # find token. Check if valid.
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
            if @current_account
              true
            else
              false
            end
          else
            false
          end
        end
      end

    end
  end
end
