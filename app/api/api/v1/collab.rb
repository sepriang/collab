require_relative 'registrations.rb'
require_relative 'activities.rb'
require_relative 'activity_transactions.rb'
require_relative 'accounts.rb'
require_relative 'push_notifications.rb'

module API
  module V1
    class Collab < Grape::API
      mount API::V1::Registrations
      mount API::V1::Accounts
	    mount API::V1::ActivityTransactions
	    mount API::V1::Activities   
      mount API::V1::PushNotifications      
      #formatting errors
      error_formatter :json, API::ErrorFormatter



    end
  end
end

