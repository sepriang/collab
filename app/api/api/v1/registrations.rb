module API
  module V1
    class Registrations < Grape::API
      version 'v1'
      format :json


      #Preferred json
        # { login : 'test@gmail.com or username', password: 'password'}
      resource :auth do
        desc "Login"
        params do
          requires :account , type: Hash, desc: "Acount" do
		        requires :login, type: String, desc: "username or email"
		        requires :password, type: String, desc: "Password"
          end
		    end
        post :login do
          login = params[:account][:login]
        	account = Account.where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
   		 	  if account
            if account.valid_password?(params[:account][:password].to_s)
              api_key = account.find_api_key
              if !api_key.access_token || api_key.expired?
                api_key.set_expires_at
                api_key.generate_access_token
              end
              api_key.save
              obj = JSON.parse(account.to_json);
              obj.delete('profile_picture') 
              obj['profile_picture'] = account.profile_picture_url(:chat)
              obj["api_key"] = account.api_keys.first
              obj["jidStr"] = "#{account.username}@localhost"
              status 201
              { :success => true, :info => "Ready to login",:data=> obj}
            else
              status 403
              { :success => false, :info => "Invalid login"}
            end
    	 	  else
            status 403
            { :success => false, :info => "Invalid login"}
    	 	  end
        end
      end

      #Preferred json
        # { email : 'test@gmail.com',current_password: 'test', password: 'test', password_confirmation: 'test'}
      # resource :auth do
      #   desc "Reset Password"
      #   params do
      #     requires :email, type: String, desc: "Email"
      #     requires :current_password, type: String, desc: "Current Password"
      #     requires :password, type: String, desc: "New Password"
      #     requires :password_confirmation, type: String, desc: "New Password Confirmation"
      #   end
      #   post :reset_password do
      #     client = Mysql2::Client.new(:host => Rails.application.secrets.old_db_host, :username => Rails.application.secrets.old_db_username,password: Rails.application.secrets.old_db_password, database: Rails.application.secrets.old_db)
      #     user_results = client.query("select * from user where user_id = '#{params[:email]}'")

      #     unless user_results.each(:as => :hash).empty?
      #       user_results.each(:as => :hash) do |row|
      #         if row["password"] == Digest::SHA1.hexdigest(params[:current_password].to_s)
      #           account = Account.find_by_email(params[:email])
      #           unless account
      #             account = Account.create!(name: row["name"],email: params[:email],password: params[:password],password_confirmation: params[:password_confirmation],bluetooth_code: row["bluetooth_code"],occupation: row[:occupation])
      #           else
      #             account.password = params[:password]
      #             account.password_confirmation = account.password
      #           end
      #           if account.save
      #             client.query("UPDATE user set password='#{Digest::SHA1.hexdigest(params[:password])}' where user_id = '#{params[:email]}'")
      #             weaers_results = client.query("SELECT * FROM wearer INNER JOIN user_group ON wearer.wearer_id = user_group.wearer_id WHERE user_group.user_id in ('#{params[:email]}')")
      #             wearers = weaers_results.each(:as => :hash)
      #             status 201
      #             return { :success => true, :info => "Reset password success", wearers: wearers, account: account}
      #           else
      #             status 405
      #             return { :success => false, :info => "Someting wrong. We wil back very soon", wearers: "", account: "" }
      #           end
      #         else
      #           status 403
      #           return { :success => false, :info => "Invalid email or passwrod", wearers: "", account: ""}
      #         end
      #       end
      #     else
      #       status 403
      #       return { :success => false, :info => "Invalid email or passwrod", wearers: "", account: ""}
      #     end
      #   end
      # end

      #Preferred json
        # account : { email : 'test@gmail.com',username: 'tware', password: 'test'}
      resource :auth do
        desc "Signup"

        params do
          requires :account , type: Hash, desc: "Acount" do
            requires :email, type: String, desc: "Email address", allow_blank: false
            requires :username, type: String, desc: "username", allow_blank: false
            requires :password, type: String, desc: "Password", allow_blank: false
          end
        end

        post :signup do
          account = Account.new(email: params[:account][:email],username: params[:account][:username],password: params[:account][:password],password_confirmation: params[:account][:password])
          if Account.all.map(&:email).include? account.email
            raise Grape::Exceptions::Validation, params: [account.email], message: "Email is duplicate",status: 403
          end

          if Account.all.map(&:username).include? account.username
            raise Grape::Exceptions::Validation, params: [account.username], message: "Username is already taken",status: 403
          end
          if account.save!
            obj = JSON.parse(account.to_json);
            obj["api_key"] = account.api_keys.first
            obj["jidStr"] = "#{account.username}@localhost"
            obj.delete('profile_picture') 
            obj['profile_picture'] = account.profile_picture_url(:chat)

            status 201
            { :success => true, :info => "Signup success",data: obj}
          else
            status 405
            { :success => false, :info => "Signup fail",data: account.errors}
          end
        end
      end

      
      resource :auth do
        desc "Logout"
        before  do
         	authenticated!
  	  	end
        get :logout do
          api_key = ApiKey.find_by_access_token(headers["Access-Token"])
          unless api_key.nil?
          	  api_key.inactive
	          if api_key.save
  	   		  	status 201
  	   		  else
  	   		  	status 401
  	   		  end
  	   	  else
  	   	  	status 403
  	   	  end
        end
      end

           helpers do
        def authenticated!
          error!('Unauthorized. Invalid or expired token.', 401) unless current_account
        end

        def current_account
          # find token. Check if valid.
          token = ApiKey.where(access_token: headers["Access-Token"]).first
          if token && !token.expired?
            @current_account = Account.find(token.account_id)
            if @current_account
              true
            else
              false
            end
          else
            false
          end
        end
      end


    end
  end
end
