class ApplicationMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'mailer'

   def invite_new_contact_email(email,account)
   	@account = account
    mail(to: email, subject: "#{@account.username} invite you to use Collab.")
  end
end
