class ApiKey < ActiveRecord::Base
	acts_as_paranoid

    #Callbacks
	before_create :generate_access_token, :set_expires_at

    #Association
  	belongs_to :account   

    #Predefined query
    default_scope { where(active: true) }

    #Validations
    validates :access_token,:expires_at,:account_id, presence: true



  	def generate_access_token
    	begin
    		self.access_token = SecureRandom.hex(40)
    	end while self.class.exists?(access_token: access_token)
  	end

   
  	def expired?
  		DateTime.now >= self.expires_at
  	end

  	def set_expires_at
  		 self.expires_at = DateTime.now+30.days
  	end
   
    def inactive
      self.active = false;
    end
end
