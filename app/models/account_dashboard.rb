class AccountDashboard < ActiveRecord::Base
	acts_as_paranoid

	#association
	belongs_to :account
	belongs_to :activity
	has_one :activity_transaction
	belongs_to :note
	#validation
  	# validates :activity_id, uniqueness: true, on: :create, scope: :account_id
end
