class ActivityTransaction < ActiveRecord::Base
	acts_as_paranoid

	#association
	belongs_to :account
	belongs_to :activity
	belongs_to :account_dashboard
	belongs_to :note
	has_many :tags
end
