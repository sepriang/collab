class Note < ActiveRecord::Base
	acts_as_paranoid
	#association 
	has_one :activity_transaction
	has_one :account_dashboard
	belongs_to :account
end
