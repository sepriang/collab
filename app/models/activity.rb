class Activity < ActiveRecord::Base
	acts_as_paranoid

  #association 
  has_many :account_dashborads
  has_many :accounts, through: :account_dashborads
  has_many :activity_transactions
  has_many :images

  #validation
  validates_uniqueness_of :name

  #callback
  before_create :default_options
  

  state_machine :state,:initial => :pending do
    # Events
    event :approve do
      transition :pending => :approved
    end
    event :pending do
      transition :contest => :pending
    end
    event :archieved do
      transition :contest => :archieved
    end

    # Transitions
    before_transition :approved => :pending do |activity|
      activity.approved_at = nil
    end

    before_transition :pending => :approved do |activity|
      activity.touch(:approved_at)
    end

    before_transition any => :unactive do |activity, transition|
      activity.touch(:archived_at)
    end 
  end

  def default_options
    self.state = 'pending'
  end

	def self.input_types
  	{
    		1 => {name: "STAR",data_type: "DECIMAL"},
    		2 => {name: "COMLETE",data_type: "BOOLEAN"}
  	}
	end

  def input_type
    return self.class.input_types[self.input_type_id.to_i]
  end
end
