class Account < ActiveRecord::Base
  acts_as_paranoid
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  attr_accessor :login

  #profile picture upload
  mount_uploader :profile_picture, AccountUploader

  #association 
  has_many :account_dashboards
  has_many :activities, through: :account_dashborads
  has_many :api_keys
  has_one :device
  has_many :activity_transactions
  has_many :notes


  #Callbacks
  before_create :find_api_key,:generate_jid_pw
  after_save :find_api_key
  after_create :register_ejabberd_account
  # after_create :connect_to_admin


  #Validations
  validates :email, presence: true
  validates :username, :presence => true, :uniqueness => { :case_sensitive => false} 

  def find_api_key
    api_key = self.api_keys.first_or_initialize
    api_key.generate_access_token
    api_key.set_expires_at
    api_key.save
    api_key
  end

  def generate_jid_pw
   self.jid_password = "#{SecureRandom.hex(12)}";
  end

  def login=(login)
    @login = login
  end

  def connect_to_admin
    im = Jabber::Simple.new("#{self.username}@localhost","#{self.jid_password}")
    # Send an authorization request to a user:
    im.add("collab@localhost")
  end

  def register_ejabberd_account
    # if Rails.env.development?
    jid = Jabber::JID.new("#{self.username}@localhost");
    # else
    #    jid = Jabber::JID.new("#{self.username}@chat.collab.com")
    # end
     ejb_client = Jabber::Client.new(jid)
     ejb_client.connect
     result = ejb_client.register(self.jid_password)
  end

  def add_new_ejabberd_buddy(account)
    # if Rails.env.development?
      # ejb_client = Jabber::Client.new(Jabber::JID.new("#{self.username}@localhost"))
      # pres = Presence.new.set_type(:subscribe).set_to("#{account.username}@localhost")
    # else
    #   ejb_client = Jabber::Client.new(Jabber::JID.new("#{self.username}@chat.collab.com"))
    #   pres = Presence.new.set_type(:subscribe).set_to("#{account.username}@chat.collab.com")
    # end
    im = Jabber::Simple.new("#{self.username}@localhost","#{self.jid_password}")
    # Send an authorization request to a user:
    im.add("#{account.username}@localhost")
  end

  def remove_ejabberd_buddy(account)
    # if Rails.env.development?
      # ejb_client = Jabber::Client.new(Jabber::JID.new("#{self.username}@localhost"))
      # pres = Presence.new.set_type(:subscribe).set_to("#{account.username}@localhost")
    # else
    #   ejb_client = Jabber::Client.new(Jabber::JID.new("#{self.username}@chat.collab.com"))
    #   pres = Presence.new.set_type(:subscribe).set_to("#{account.username}@chat.collab.com")
    # end
    im = Jabber::Simple.new("#{self.username}@localhost","#{self.jid_password}")
    # Send an authorization request to a user:
    im.remove("#{account.username}@localhost")
  end

  def login
    @login || self.username || self.email
  end
end
