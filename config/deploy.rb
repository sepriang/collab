require 'active_support/all'
# config valid only for Capistrano 3.1
lock ' 3.4.0'

# setup multistage
set :stages, %w(staging production)
set :default_stage, "production"

set :application, 'collab'
set :repo_url, 'git@bitbucket.org:kotharngal/collab.git'


set :user, "root"
# set :whenever_environment, "production"
# set :whenever_roles,        ->{ [:db, :app, :web] }

# Default branch is :master
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/home/azureuser/apps/collab" 

# Default value for :scm is :git
 set :scm, :git

# Default value for :format is :pretty
 set :format, :pretty

# Default value for :log_level is :debug
 set :log_level, :debug

set :use_sudo, false

# Default value for :pty is false
 set :pty, true


set :bundle_binstubs, nil
# set :rvm_type, :user                     # Defaults to: :auto
# set :rvm_ruby_version, '2.2.1'      # Defaults to: 'default'
# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
# set :linked_files, fetch(:linked_files, []).push('config/smtp.yml')

# Default value for linked_dirs is []
 set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5





