CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => 'AWS',                        # required
    :aws_access_key_id      => Rails.application.secrets.s3_provider_key,                     # required
    :aws_secret_access_key  => Rails.application.secrets.s3_provider_secret,                       # required
    :region                 => Rails.application.secrets.s3_region                 # optional, defaults to 'us-east-1'
  }
  config.fog_directory  = 'collab.dev'                          # required
  config.fog_public     = false                                        # optional, defaults to true
  config.fog_attributes = {'Cache-Control'=>"max-age=#{365.day.to_i}"} # optional, defaults to {}
end


