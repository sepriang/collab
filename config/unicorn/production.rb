working_directory "/home/azureuser/apps/collab/current"
pid "/home/azureuser/apps/collab/current/tmp/pids/unicorn.pid"
stderr_path "/home/azureuser/apps/collab/shared/log/unicorn.log"
stdout_path "/home/azureuser/apps/collab/shared/log/unicorn.log"

listen "/home/azureuser/apps/collab/current/tmp/unicorn.collab.slock", backlog: 256
worker_processes 2
timeout 60

preload_app true

before_fork do |server, worker|
  # the following is highly recomended for Rails + "preload_app true"
  # as there's no need for the master process to hold a connection
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end

  # Before forking, kill the master process that belongs to the .oldbin PID.
  # This enables 0 downtime deploys.
  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end
end