namespace :secret do

  	desc "Generate the secrets.yml configuration file."
    task :setup do
    	on roles(:app) do
      		execute :sudo, "mkdir -p #{shared_path}/config"
      		template "secrets.yml.erb", "#{shared_path}/config/secrets.yml"
    	end
  	end
  	after "deploy:install", "secret:setup"

  	desc "Symlink the secrets.yml file into latest release"
  	task :symlink do
    	on roles(:app) do
      		execute :sudo, "ln -nfs #{shared_path}/config/secrets.yml #{release_path}/config/secrets.yml"
    	end
  	end
  	after "deploy:finishing", "secret:symlink"
end


