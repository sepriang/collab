set_default :ruby_version, File.read(File.expand_path('../../.ruby-version', __FILE__))
set_default :rbenv_bootstrap, 'bootstrap-ubuntu-12-04'

namespace :git do
  desc "Install rbenv, Ruby, and the Bundler gem"
  task :install do
    on roles(:app) do
      execute :sudo, "apt-get -y -q install curl git-core"
    end
  end
  # afte "deploy:install", "rbenv:install"
end

  #     execute :sudo, "curl -L https://raw.github.com/fesplugas/rbenv-installer/master/bin/rbenv-installer | bash"
  #     bashrc = <<-BASHRC
  # if [ -d $HOME/.rbenv ]; then 
  #   export PATH="$HOME/.rbenv/bin:$PATH" 
  #   eval "$(rbenv init -)" 
  # fi
  # BASHRC
  #     put bashrc, "/tmp/rbenvrc"
  #     run "cat /tmp/rbenvrc ~/.bashrc > ~/.bashrc.tmp"
  #     run "mv ~/.bashrc.tmp ~/.bashrc"
  #     run %q{export PATH="$HOME/.rbenv/bin:$PATH"}
  #     run %q{eval "$(rbenv init -)"}
  #     run "rbenv #{rbenv_bootstrap}"
  #     run "rbenv install --force #{ruby_version}"
  #     run "RBENV_VERSION=#{ruby_version} gem install bundler --no-ri --no-rdoc"
  #     run "rbenv rehash"
  #   end
