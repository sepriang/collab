def template(from, to, options = { })
  template_path = File.expand_path("../templates/#{from}", __FILE__)
  template = ERB.new(File.new(template_path).read).result(binding)
  upload! StringIO.new(template), to
end

def set_default(name, *args, &block)
  if name
    set(name, *args, &block) 
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end

namespace :deploy do
  desc "Configures a vanilla centos server with all the software we need"
  task :install do
    on roles :all do
      execute :sudo, "apt-get -y -q update"
      execute :sudo, "apt-get -y -q install cloud-utils"
      execute :sudo, "apt-get -y -q install python-software-properties"
      execute :sudo, "apt-get -y -q install libmysqlclient-dev mysql-client-5.5"
      execute :sudo, "apt-get -y -q install libpq-dev"
      execute :sudo, "apt-get -y -q install libxml2 libxml2-dev libxslt-dev"
      execute :sudo, "apt-get -y -q install imagemagick libmagickwand-dev"
      execute :sudo, "apt-get -y -q install nodejs"
      execute :sudo, "apt-get -y install ejabberd"
    end
  end
end

