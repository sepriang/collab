namespace :mysql do
  desc "Generate the database.yml configuration file."
  task :setup do
    on roles(:app) do
      execute :sudo, "mkdir -p #{shared_path}/config"
      template "database.yml.erb", "#{shared_path}/config/database.yml"
    end
  end
  after "deploy:install", "mysql:setup"
  # after "deploy:install", "mysql:setup"

  desc "Symlink the database.yml file into latest release"
  task :symlink do
    on roles(:app) do
      execute :sudo, "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end
  end
  after "deploy:finishing", "mysql:symlink"
  # after "deploy:install", "mysql:symlink"
end


